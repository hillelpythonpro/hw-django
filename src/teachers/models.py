from django.db import models
from faker import Faker
from core.models import Person

# Create your models here.
groups = ["1A", "1B", "2A", "2B", "3A", "3B", "4A", "4B", None]
subjects = ["Math", "Law", "Astronomy", "History", "Chemistry", "Physics", "Sport"]


class Teacher(Person):
    subject = models.CharField(max_length=120)
    mentor_status = models.BooleanField(default=False)
    group = models.ManyToManyField('groups.Group', related_name='teachers')
    avatar = models.ImageField(upload_to="avatar/teachers/", default="avatar/default.jpg")

    def __str__(self):
        return (f"{self.first_name} {self.last_name} {self.email} {self.date_of_birth} {self.phone_number}"
                f"{self.address}")

    @classmethod
    def generate_teachers(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            teacher = Teacher(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                phone_number=faker.phone_number(),
                address=faker.address(),
                date_of_birth=faker.date_of_birth(minimum_age=20, maximum_age=60),
                subject=faker.random_element(subjects),
                mentor_status=faker.random_element([True, False]),
                mentor_group=faker.random_element(groups),
            )
            teacher.save()
