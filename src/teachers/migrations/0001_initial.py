# Generated by Django 4.2.11 on 2024-04-09 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("groups", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Teacher",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("first_name", models.CharField(max_length=120)),
                ("last_name", models.CharField(max_length=120)),
                ("email", models.EmailField(max_length=150)),
                ("date_of_birth", models.DateField(blank=True, null=True)),
                (
                    "phone_number",
                    models.CharField(blank=True, max_length=30, null=True),
                ),
                ("address", models.CharField(blank=True, max_length=120, null=True)),
                ("subject", models.CharField(max_length=120)),
                ("mentor_status", models.BooleanField(default=False)),
                (
                    "group",
                    models.ManyToManyField(related_name="teachers", to="groups.group"),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
