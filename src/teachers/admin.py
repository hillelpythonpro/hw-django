from django.contrib import admin

from core.utils.admin_filter import TeacherListFilterGroup
from teachers.models import Teacher


class GroupInLine(admin.TabularInline):
    model = Teacher.group.through
    extra = 0


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'email', 'date_of_birth', 'age', 'phone_number')
    list_display_links = ('id', 'first_name', 'last_name')
    search_fields = ('first_name', 'email', 'phone_number')
    ordering = ('id', 'first_name', 'last_name')
    inlines = [GroupInLine]
    list_filter = (TeacherListFilterGroup,)
    fieldsets = (
        ('Personal info', {
            'fields': (('avatar',),
                       ('first_name', 'last_name'),
                       ('email', 'date_of_birth'),),
            'description': 'General info'
        }),
        ('Additional info', {
            'classes': ('collapse',),
            'fields': (('mentor_status', 'group'),
                       ('address', 'phone_number'),),
        })
    )
