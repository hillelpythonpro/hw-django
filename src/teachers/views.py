from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from django.db.models import Q
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from teachers.forms import TeacherForm
from teachers.models import Teacher


# Create your views here.


class GetTeachers(LoginRequiredMixin, ListView):
    template_name = "teachers/list.html"
    context_object_name = "teachers"
    queryset = Teacher.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        search_query = self.request.GET.get('q')
        if search_query:
            or_filter = Q()
            search_fields = [
                "first_name",
                "last_name",
                "email",
                "phone_number",
                "date_of_birth",
                "subject",
                "group__name",
            ]
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search_query})
            queryset = queryset.filter(or_filter)
        return queryset


class CreateTeacher(LoginRequiredMixin, CreateView):
    template_name = "common/create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class UpdateTeacher(LoginRequiredMixin, UpdateView):
    template_name = "common/update.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")
    queryset = Teacher.objects.all()


class DeleteTeacher(LoginRequiredMixin, DeleteView):
    template_name = 'common/delete.html'
    success_url = reverse_lazy("teachers:get_teachers")
    queryset = Teacher.objects.all()
