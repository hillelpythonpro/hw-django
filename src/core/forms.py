from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from core.models import UserProfile


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ["username", "first_name", "last_name", "email", "password1", "password2"]


class UserForm(ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["username", "first_name", "last_name", "email"]


class UserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        exclude = ['user']
        fields = '__all__'
