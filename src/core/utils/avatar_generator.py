import requests
import os


def download_images(num, name):
    if not os.path.exists(f'../user_content/avatar/{name}'):
        os.makedirs(f'../user_content/avatar/{name}')

    for i in range(num):
        response = requests.get('https://thispersondoesnotexist.com', headers={'User-Agent': 'Mozilla/5.0'})

        with open(f'../user_content/avatar/{name}/person_{i + 1}.jpg', 'wb') as f:
            f.write(response.content)
            print(f"Изображение {i + 1} загружено")
