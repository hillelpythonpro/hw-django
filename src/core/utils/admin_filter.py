from django.contrib import admin


class GroupListFilter(admin.SimpleListFilter):
    title = 'Group name'
    parameter_name = 'group_name'

    def lookups(self, request, model_admin):

        return (
            ('1A', '1A'),
            ('1B', '1B'),
            ('2A', '2A'),
            ('2B', '2B'),
            ('3A', '3A'),
            ('3B', '3B'),
            ('4A', '4A'),
            ('4B', '4B'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(group_id__name=self.value())


class GroupSubjectListFilter(admin.SimpleListFilter):
    title = 'Group subject'
    parameter_name = 'focus_on_subject'

    def lookups(self, request, model_admin):

        return (
            ('Math', 'Math'),
            ('Law', 'Law'),
            ('Astronomy', 'Astronomy'),
            ('History', 'History'),
            ('Chemistry', 'Chemistry'),
            ('Physics', 'Physics'),
            ('Sport', 'Sport'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(group_id__focus_on_subject=self.value())


class StudentQuantityFilter(admin.SimpleListFilter):
    title = 'Student quantity'
    parameter_name = 'student_quantity'

    def lookups(self, request, model_admin):
        return (
            ('1-5', '1 - 5'),
            ('6-10', '6 - 10'),
            ('11-20', '11 - 20'),
            ('21+', '21+'),
        )

    def queryset(self, request, queryset):
        if self.value() == '1-5':
            return queryset.filter(students_quantity__range=(1, 5))
        if self.value() == '6-10':
            return queryset.filter(students_quantity__range=(6, 10))
        if self.value() == '11-20':
            return queryset.filter(students_quantity__range=(11, 20))
        if self.value() == '21+':
            return queryset.filter(students_quantity__gte=21)


class TeacherListFilterGroup(admin.SimpleListFilter):
    title = 'Group'
    parameter_name = 'teacher_group_name'

    GROUP_CHOICES = {
        '1A': '1A',
        '1B': '1B',
        '2A': '2A',
        '2B': '2B',
        '3A': '3A',
        '3B': '3B',
        '4A': '4A',
        '4B': '4B',
    }

    def lookups(self, request, model_admin):
        return self.GROUP_CHOICES.items()

    def queryset(self, request, queryset):
        value = self.value()
        if value in self.GROUP_CHOICES:
            return queryset.filter(group__name=self.GROUP_CHOICES[value])
        return queryset
