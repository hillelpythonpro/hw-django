from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from core.models import UserProfile


class UserProfileInLine(admin.TabularInline):
    model = UserProfile


class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email', 'is_staff',
                    'is_superuser', 'date_joined', 'last_login', 'is_active')
    search_fields = ('username', 'first_name', 'last_name', 'email')
    date_hierarchy = 'date_joined'
    inlines = [UserProfileInLine]


admin.site.unregister(User)

admin.site.register(User, CustomUserAdmin)
