from datetime import datetime

from django.contrib.auth import get_user_model
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

user_type = (
    ("Teacher", "Teacher"),
    ("Student", "Student"),
    ("Mentor", "Mentor"),
)

gender = (
    ("Male", "Male"),
    ("Female", "Female"),
    ("Prefer not to say", "Prefer not to say"),
)


class Person(models.Model):
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    email = models.EmailField(max_length=150)
    date_of_birth = models.DateField(null=True, blank=True)
    phone_number = models.CharField(max_length=30, blank=True, null=True)
    address = models.CharField(max_length=120, blank=True, null=True)

    def age(self):
        return datetime.now().year - self.date_of_birth.year

    class Meta:
        abstract = True


class UserProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='user_profile')
    phone_number = PhoneNumberField(blank=True, null=True)
    address = models.CharField(max_length=120, blank=True, null=True)
    gender = models.CharField(max_length=20, choices=gender, blank=True, null=True)
    nationality = models.CharField(max_length=120, blank=True, null=True)
    date_of_birth = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=10, choices=user_type, blank=True, null=True)
    profile_pic = models.ImageField(upload_to="avatar/users/", default="avatar/default.jpg")
    certificates = models.FileField(upload_to="certificates/users/", blank=True, null=True)
    bio = models.CharField(max_length=2000, blank=True, null=True)
