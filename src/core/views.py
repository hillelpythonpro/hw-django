from django.conf import settings
from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, RedirectView, UpdateView, DetailView

from core.forms import UserRegistrationForm, UserProfileForm, UserForm
from core.models import UserProfile
from core.services.emails import send_registration_email
from core.utils.token_generator import TokenGenerator


class UserLoginView(LoginView):
    pass


class UserLogoutView(LogoutView):
    next_page = reverse_lazy("login")


class UserRegistrationView(CreateView):
    template_name = "registration/registration.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        UserProfile.objects.create(user=self.object)

        send_registration_email(user_instance=self.object, request=self.request)
        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, ValueError, TypeError):
            return HttpResponse("Wrong data!")
        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)


def send_email(request: HttpRequest) -> HttpResponse:
    send_mail(
        subject="",
        message="",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[
            settings.EMAIL_HOST_USER,
            ''
        ],
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
    )
    return HttpResponse("success")


class UserProfileView(LoginRequiredMixin, DetailView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = "user/user_profile.html"

    def get_object(self, queryset=None):
        user_id = self.kwargs.get("user_id")
        user_profile = get_object_or_404(UserProfile.objects.select_related("user"), user_id=user_id)
        return user_profile


class UpdateUserProfileView(UpdateView):
    form_class = UserProfileForm
    user_form_class = UserForm
    model = UserProfile
    template_name = 'user/update_user_profile.html'

    def get_object(self, queryset=None):
        user_id = self.kwargs.get("user_id")
        user_profile = get_object_or_404(UserProfile.objects.select_related("user"), user_id=user_id)
        return user_profile

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["user_form"] = self.user_form_class(instance=self.object.user)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        user_form = self.user_form_class(request.POST, instance=self.object.user)
        if form.is_valid() and user_form.is_valid():
            return self.form_valid(form, user_form)
        else:
            return self.form_invalid(form, user_form)

    def form_valid(self, form, user_form):
        self.object = form.save()
        user_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, user_form):
        return self.render_to_response(self.get_context_data(form=form, user_form=user_form))

    def get_success_url(self):
        user_id = self.kwargs.get("user_id")
        return reverse_lazy("user_profile", kwargs={"user_id": user_id})
