from django.contrib import admin

from core.utils.admin_filter import GroupSubjectListFilter, GroupListFilter
from students.models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "email", "date_of_birth", "age", "phone_number", "group")
    list_display_links = ("first_name", "last_name")
    search_fields = ("email", "first_name", "last_name", "phone_number",)
    ordering = ("first_name", "last_name")
    list_filter = (GroupSubjectListFilter, GroupListFilter)
    fieldsets = (
        ('Personal info', {
            'fields': (('avatar',),
                       ('first_name', 'last_name'),
                       ('email', 'date_of_birth'),),
            'description': 'General info'
        }),
        ('Additional info', {
            'classes': ('collapse',),
            'fields': (('enrollment', 'group'),
                       ('address', 'phone_number'),
                       ('resume',)),
        })
    )
