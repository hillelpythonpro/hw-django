from django.db import models
from faker import Faker
from core.models import Person
from uuid import uuid4
# Create your models here.


class Student(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    enrollment = models.BooleanField(default=False)
    group = models.ForeignKey("groups.Group", on_delete=models.CASCADE, default=None, related_name="students")
    avatar = models.ImageField(upload_to="avatar/students/", default="avatar/default.jpg")
    resume = models.FileField(upload_to="resume/students/", blank=True, null=True)

    def __str__(self):
        return (f"{self.first_name} {self.last_name} {self.email} {self.date_of_birth} {self.phone_number}"
                f"{self.address} ({self.uuid})")

    @classmethod
    def generate_students(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            student = Student(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                phone_number=faker.phone_number(),
                address=faker.address(),
                date_of_birth=faker.date_of_birth(minimum_age=16, maximum_age=60),
                enrollment=faker.random_element([True, False]),
            )
            student.save()
