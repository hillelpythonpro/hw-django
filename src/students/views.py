from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.db.models import Q
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from students.forms import StudentForm
from students.models import Student


# Create your views here.

def custom_404_view(request, exception):
    return render(request, "errors/404.html", status=404)


class GetStudents(LoginRequiredMixin, ListView):
    template_name = "students/list.html"
    context_object_name = "students"
    queryset = Student.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        search_query = self.request.GET.get('q')
        if search_query:
            or_filter = Q()
            search_field = [
                "first_name",
                "last_name",
                "email",
                "phone_number",
                "group__name",
                "date_of_birth",
            ]
            for field in search_field:
                or_filter |= Q(**{f"{field}__icontains": search_query})
            queryset = queryset.filter(or_filter)
        return queryset


class CreateStudent(LoginRequiredMixin, CreateView):
    template_name = "common/create.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")


class UpdateStudent(LoginRequiredMixin, UpdateView):
    template_name = "common/update.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")
    queryset = Student.objects.all()
    pk_url_kwarg = 'uuid'


class DeleteStudent(LoginRequiredMixin, DeleteView):
    template_name = 'common/delete.html'
    success_url = reverse_lazy("students:get_students")
    queryset = Student.objects.all()
    pk_url_kwarg = 'uuid'
