# Generated by Django 4.2.11 on 2024-04-13 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0003_alter_student_group"),
    ]

    operations = [
        migrations.AddField(
            model_name="student",
            name="avatar",
            field=models.ImageField(
                default="avatar/default.jpg", upload_to="avatar/students/"
            ),
        ),
        migrations.AddField(
            model_name="student",
            name="resume",
            field=models.FileField(blank=True, null=True, upload_to="resume/students/"),
        ),
    ]
