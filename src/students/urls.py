from django.urls import path
from students.views import CreateStudent, UpdateStudent, DeleteStudent, GetStudents

app_name = "students"

urlpatterns = [
    path('', GetStudents.as_view(), name='get_students'),
    path('create/', CreateStudent.as_view(), name='create_student'),
    path('update/<uuid:uuid>/', UpdateStudent.as_view(), name='update_student'),
    path('delete/<uuid:uuid>/', DeleteStudent.as_view(), name='delete_student'),
]
