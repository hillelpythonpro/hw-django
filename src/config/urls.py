"""
URL configuration for config project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
import debug_toolbar
import grappelli
from index import IndexView
from core.views import UserLoginView, UserLogoutView, UserRegistrationView, send_email, ActivateUser, UserProfileView, \
    UpdateUserProfileView

if settings.DEBUG:
    import debug_toolbar  # NOQA: F811

handler404 = 'students.views.custom_404_view'

urlpatterns = [
    path("__debug__/", include(debug_toolbar.urls)),
    path('grappelli/', include('grappelli.urls')),
    path("admin/", admin.site.urls),
    path("", IndexView.as_view(), name="index"),
    path("students/", include('students.urls')),
    path("groups/", include('groups.urls')),
    path("teachers/", include('teachers.urls')),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("register/", UserRegistrationView.as_view(), name="register"),
    path("send_email/", send_email, name="send_email"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    path("profile/<int:user_id>/", UserProfileView.as_view(), name="user_profile"),
    path("profileupdate/<int:user_id>/", UpdateUserProfileView.as_view(), name="update_user_profile"),
    path("oauth/", include("social_django.urls", namespace="social"))
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
