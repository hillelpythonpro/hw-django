from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.db.models import Q
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView

from groups.forms import GroupForm
from groups.models import Group


# Create your views here.


class GetGroups(LoginRequiredMixin, ListView):
    template_name = "groups/list.html"
    context_object_name = "groups"
    queryset = Group.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        search_query = self.request.GET.get('q')
        if search_query:
            or_filter = Q()
            search_fields = [
                "name",
                "focus_on_subject",
                "students_quantity",
                "teachers__first_name",
                "teachers__last_name",
            ]
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search_query})
            queryset = queryset.filter(or_filter)
        return queryset


class CreateGroup(LoginRequiredMixin, CreateView):
    template_name = "common/create.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")


class UpdateGroup(LoginRequiredMixin, UpdateView):
    template_name = "common/update.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")
    queryset = Group.objects.all()


class DeleteGroup(LoginRequiredMixin, DeleteView):
    template_name = 'common/delete.html'
    success_url = reverse_lazy("groups:get_groups")
    queryset = Group.objects.all()


class GetInfoGroup(LoginRequiredMixin, DetailView):
    template_name = "groups/group_info.html"
    context_object_name = "group"
    queryset = Group.objects.all()
