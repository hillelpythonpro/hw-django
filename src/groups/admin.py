from django.contrib import admin

from core.utils.admin_filter import StudentQuantityFilter, GroupListFilter
from groups.models import Group
from students.models import Student
from teachers.models import Teacher


class StudentInline(admin.TabularInline):
    model = Student


class TeacherInline(admin.TabularInline):
    model = Teacher.group.through
    extra = 1


@admin.action(description="Set to zero student count of group")
def set_to_zero(admin_model, request, queryset):
    queryset.update(students_quantity=0)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "focus_on_subject", "students_quantity")
    list_display_links = ("id", "name")
    ordering = ("id",)
    inlines = [StudentInline, TeacherInline]
    search_fields = ("name", "focus_on_subject", "students_quantity")
    actions = [set_to_zero]
    list_filter = (StudentQuantityFilter,)
