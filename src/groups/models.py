from django.db import models
from faker import Faker

# Create your models here.
groups = ["1A", "1B", "2A", "2B", "3A", "3B", "4A", "4B"]
subjects = ["Math", "Law", "Astronomy", "History", "Chemistry", "Physics", "Sport"]


class Group(models.Model):
    name = models.CharField(max_length=120)
    focus_on_subject = models.CharField(max_length=120)
    students_quantity = models.PositiveSmallIntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return f"{self.name} {self.id}"

    @classmethod
    def generate_groups(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            group = Group(
                name=faker.random_element(groups),
                focus_on_subject=faker.random_element(subjects),
                students_quantity=faker.random_int(min=1, max=30),
                mentor_first_name=faker.first_name(),
                mentor_last_name=faker.last_name(),
                mentor_email=faker.email(),
            )
            group.save()
