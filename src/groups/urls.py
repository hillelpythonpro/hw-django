from django.urls import path
from groups.views import GetGroups, CreateGroup, UpdateGroup, DeleteGroup, GetInfoGroup

app_name = "groups"

urlpatterns = [
    path('', GetGroups.as_view(), name='get_groups'),
    path('create/', CreateGroup.as_view(), name='create_group'),
    path('update/<int:pk>/', UpdateGroup.as_view(), name='update_group'),
    path('delete/<int:pk>/', DeleteGroup.as_view(), name='delete_group'),
    path('info/<int:pk>/', GetInfoGroup.as_view(), name='group_info'),
]
